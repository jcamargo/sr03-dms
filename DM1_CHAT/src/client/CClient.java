package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import threads.*;
/**
 * CClient est une classe qui joue le role d'un client qui etablit
 * une connexion avec le serveur.
 * 
 * Au momment de lancer la classe, celle-ci etablira une connexion avec le
 * serveur, apres il nous sera demande de saisir un surnom par lequel on
 * s'indentifiera dans le chat. Une fois que le surnom est accepte par le serveur
 * on passe a implementer 2 threads.
 * 
 * Le premiere cherche a permettre au client d'ecrire des messages en parallele, Le
 * deuxieme a pour objectif d'entendre tout message provenant d'un autre client.
 * 
 * @author Alice
 *
 */
public class CClient {

	public static void main(String[] args) throws UnknownHostException, IOException {

		Socket communicationSocket = new Socket("localhost", 3000);
		DataInputStream in = new DataInputStream(communicationSocket.getInputStream());
		DataOutputStream out = new DataOutputStream(communicationSocket.getOutputStream());
		CWriteMessageThread writeMessagesThread = null;
		CIntercepteThread interceptMessagesThread = null;
		System.out.println("Bienvenue dans le chat! Entrez votre surnom: ");
		Scanner scanner = new Scanner(System.in);
		String clientName = scanner.nextLine();
		String nameAv = "";
		out.writeUTF(clientName);

		do {

			nameAv = in.readUTF();
			if (nameAv.equals("no")) {
				System.out.println("Le surnom est deja utilise, saisissez un autre svp ");
				clientName = scanner.nextLine();
				out.writeUTF(clientName);
			}
		} while (!nameAv.equals("yes"));
		System.out.println("Votre surnom est: " + clientName);
		writeMessagesThread = new CWriteMessageThread(communicationSocket);
		interceptMessagesThread = new CIntercepteThread(communicationSocket, clientName);
		writeMessagesThread.start();
		interceptMessagesThread.start();

	}

}
