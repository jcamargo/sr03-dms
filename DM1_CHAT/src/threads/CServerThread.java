package threads;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import server.CServer;

/**
 * CServerThread est la classe qui est utilise par la classe  CServer pour lancer un
 * proccessus en parallele capable de recevoir les messages emergant d'un des 
 * clients qui est connecte au serveur.
 * 
 * @author Julio
 *
 */
public class CServerThread extends Thread {

	private Map<String, Socket> connectedClients = null;
	private Socket client = null;
	private String clientName = null;
	private volatile boolean finishListen = false;
	private DataInputStream in = null;
	private DataOutputStream out = null;

	public CServerThread(Socket currentClient, String clientName) throws IOException {
		this.clientName = clientName;
		this.client = currentClient;
		this.in = new DataInputStream(currentClient.getInputStream());
		this.out = new DataOutputStream(currentClient.getOutputStream());
	}

	public Socket getCommunicationSocket() {
		return client;
	}

	public String getUserName() {
		return clientName;
	}

	public boolean getFinishListen() {
		return this.finishListen;
	}

	public void setFinishListen(boolean result) {
		this.finishListen = result;
	}
	/**
	 * L'objectif de la methode c'est d'envoyer le message recu par le serveur a
	 * tous les clients connectes a celui-ci.
	 * 
	 * @param message contenu du message qui doit etre envoye
	 * @param sender  surnom du clinet qui envoie le message
	 * @throws IOException
	 */
	public void spreadMessageClient(String message, String sender) throws IOException {

		DataOutputStream outFunction = null;
		String separador = Pattern.quote("|");
		String[] messageReceived = message.split(separador);
		connectedClients = CServer.getMapClients();
		if (messageReceived[1].equals("yes")) {
			connectedClients.remove(getUserName());
			CServer.setMapClients(connectedClients);
		}
		for (Map.Entry<String, Socket> client : connectedClients.entrySet()) {
			if (!client.getKey().equals(sender)) {
				outFunction = new DataOutputStream(((Socket) client.getValue()).getOutputStream());
				if (message.equals("exit")) {
					outFunction.writeUTF(sender + "|" + message);
				} else {
					outFunction.writeUTF(sender + "|" + message);
				}

			}

		}

	}


	
	/**
	 * Cette methode libere la socket et  les flux d'E/S
	 * @return
	 */

	public void closeConnections() throws IOException {
		this.in.close();
		this.out.close();
		this.client.close();
	}

	@Override
	public void run() {
		String receivedMessage = "";

		try {

			while (!getFinishListen()) {
				receivedMessage = in.readUTF();
				if (receivedMessage.equals("exit")) {
					setFinishListen(true);
					spreadMessageClient(getUserName() + " s'est deconnecte|yes", "Server");

				} else {
					spreadMessageClient(receivedMessage + "|no", getUserName());
				}

			}

			closeConnections();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
