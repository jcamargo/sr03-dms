package threads;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * CWriteMessageThread est une classe qui herite de Thread dont l'objectif  est 
 * de lancer un processus en parallele capable de permettre au client 
 * d'ecrire un message a envoyer aux clients pendant qu'il attend des messages issues d'eux. 
 * @author Alice
 *
 */
public class CWriteMessageThread extends Thread {

	Socket client = null;
	DataInputStream in = null;
	DataOutputStream output = null;
	volatile boolean finishThread = false;

	public CWriteMessageThread(Socket client) throws IOException {
		this.client = client;
		this.in = new DataInputStream(client.getInputStream());
		this.output = new DataOutputStream(client.getOutputStream());
	}

	public Socket getClientSocket() {
		return this.client;
	}

	@Override

	public void run() {
		String messageReceived;
		Scanner scanner = new Scanner(System.in);
		String newMessage = "";
		try {
			while (!getFinishThread()) {

				newMessage = scanner.nextLine();
				output.writeUTF(newMessage);
				if (newMessage.equals("exit")) {
					setFinishThread(true);

				}

			}

			closeConnections();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public boolean getFinishThread() {
		return this.finishThread;
	}

	public void setFinishThread(boolean result) {
		this.finishThread = result;
	}
	
	
	/**
	 * Cette methode libere la socket et  les flux d'E/S
	 * @return
	 */

	public void closeConnections() throws IOException {
		this.in.close();
		this.output.close();
		this.client.close();
	}
}
