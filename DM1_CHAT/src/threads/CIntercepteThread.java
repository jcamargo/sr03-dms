package threads;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Vector;
import java.util.regex.Pattern;

import server.CServer;

/**
 * CIntercepteThread est la classe qui est utilise par la classe CClient pour lancer un
 * proccessus en parallele capable de recevoir les messages emergant des
 * differents clients.
 * 
 * 
 * 
 * @author Julio
 *
 */
public class CIntercepteThread extends Thread {

	Socket client = null;
	String userName = "";
	DataInputStream in = null;
	DataOutputStream output = null;
	boolean receiveMessages = true;

	/**
	 * 
	 * @param client         socket de communication du client
	 * @param clientNickName surnom du client
	 * @throws IOException
	 */
	public CIntercepteThread(Socket client, String clientNickName) throws IOException {
		this.client = client;
		this.in = new DataInputStream(client.getInputStream());
		this.output = new DataOutputStream(client.getOutputStream());
		this.userName = clientNickName;
	}

	public Socket getClientSocket() {
		return this.client;
	}

	public String getUserName() {
		return userName;
	}

	public boolean getReceiveMessages() {
		return this.receiveMessages;
	}

	public void setReceiveMessages(boolean res) {
		this.receiveMessages = res;
	}

	public void closeConnections() throws IOException {
		this.in.close();
		this.output.close();
		this.client.close();
	}

	@Override

	public void run() {
		String[] messageReceived = null;
		String separador = Pattern.quote("|");
		try {
			while (getReceiveMessages()) {

				messageReceived = in.readUTF().split(separador);
				if (messageReceived[2].equals("yes") && messageReceived[0].equals(getUserName())) {
					System.out.println("Je vais pas ecouter plus de messages");
					setReceiveMessages(false);
				}
				System.out.println(messageReceived[0] + " a dit: " + messageReceived[1]);

			}
			closeConnections();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
