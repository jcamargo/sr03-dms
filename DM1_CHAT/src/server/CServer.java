package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;

import threads.*;
/**
 * CServer c'est une classe qui a ete implementee pour lancer un processus sur
 * lequel seront recues toutes les demandes de connexions des clients qui
 * essaient d'etablir une connexion avec le serveur.
 * 
 * Dans la method statique main , le serveur attend tous les demandes. S'il
 * recoit une demande celui-ci commencera par verifier si le surnom saisi par
 * le client n'a pas ete encore pris par un autre client. Une fois que le client
 * est connecte la classe lance le thread dans lequel on ecoutera tout message issu
 * du client.
 * 
 * @author Julio
 *
 */
public class CServer {

	public static Map<String, Socket> clientsSockets = new HashMap<String, Socket>();

	public static void main(String[] args) throws IOException {

		ServerSocket connectionSocket = new ServerSocket(3000);
		DataInputStream in = null;
		DataOutputStream out = null;
		String nickNameClient = "";
		CServerThread clientThread = null;
		
		while (true) {
			Socket communicationSocket = connectionSocket.accept();
			out = new DataOutputStream(communicationSocket.getOutputStream());
			in = new DataInputStream(communicationSocket.getInputStream());
			nickNameClient = in.readUTF();

			while (!CServer.isNickNameAvailable(nickNameClient)) {
				out.writeUTF("no");
				nickNameClient = in.readUTF();
			}
			out.writeUTF("yes");
			clientsSockets.put(nickNameClient, communicationSocket);
			CServer.spreadMessageClient("", nickNameClient, communicationSocket);

			clientThread = new CServerThread(communicationSocket, nickNameClient);
			clientThread.start();
		}
	}

	/**
	 * La methode retourne l'instance de la liste de sockets des clients connectes
	 * au serveur
	 * 
	 * @return objet de type Map qui contient les clients connectes au serveur
	 */
	public static Map<String, Socket> getMapClients() {
		return clientsSockets;
	}

	/**
	 * Modification de la liste courante des clients connectes au serveur
	 * 
	 */
	public static void setMapClients(Map<String, Socket> newMappingClietns) {
		CServer.clientsSockets = newMappingClietns;
	}

	/**
	 * Compare le surnom donne comme parametre avec tous les autres surnoms deja
	 * enregistre par le serveur.
	 * 
	 * @param newUser surnom du nouveau utilisateur qui essaie se connecter au
	 *                serveur
	 * @return true si le surnom n'a pas ete encore pris par un autre client
	 */
	public static boolean isNickNameAvailable(String newUser) {
		boolean clientExists = true;
		if (clientsSockets.size() == 0)
			return true;
		for (Map.Entry<String, Socket> client : clientsSockets.entrySet()) {
			if (newUser.equals(client.getKey())) {
				clientExists = false;
			}
		}
		return clientExists;
	}

	/**
	 * L'objectif de la methode est d'envoyer le message recu par le serveur a
	 * tous les clients connectes a celui-ci.
	 * 
	 * @param message             contenu du message qui doit etre envoye
	 * @param sender              surnom du clinet qui envoie le message
	 * @param newCommSocketClient objet socket de communication du client qui
	 *                            envoyera le message
	 * @throws IOException
	 */

	public static void spreadMessageClient(String message, String sender, Socket newCommSocketClient)
			throws IOException {
		DataOutputStream out = null;

		if (newCommSocketClient != null) {
			out = new DataOutputStream(newCommSocketClient.getOutputStream());
		}
		Map<String, Socket> connectedClients = null;
		connectedClients = CServer.getMapClients();
		for (Map.Entry<String, Socket> client : connectedClients.entrySet()) {
			if (!client.getKey().equals(sender)) {
				out = new DataOutputStream(((Socket) client.getValue()).getOutputStream());
				out.writeUTF("Server|" + sender + " a  rejoint la conversation|no");

			}

		}
	}
}
